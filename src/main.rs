#[macro_use] extern crate blocks;
use blocks::core::core;
//use blocks::core::core::models_core;
//use blocks::core::core::Module;

use std::env;

models_core! {
    struct Block {
        index: u64,
        previous_hash: String,
        timestamp: u64,
        data: String,
        hash: String
    }
}


fn main() {
//    for i in Block::field_names(){
//        println!("{}", i.to_string());
//
//    }
//    let args: Vec<_> = env::args().collect();
    // block genesis
    let block = core::Core {
        models:Block::create()
    };

    block.create();
//    core::Block(args);
}

