use core::database::table;
use core::database::table::ModuleTable;
use core::database::fields;
// Макрос для взятие имен полей структуры и создания "корневых" методов
#[macro_export]
macro_rules! models_core {
    (struct $name:ident { $($fname:ident : $ftype:ty),* }) => {
        struct $name {
            $($fname : $ftype),*
        }
        impl $name {
            fn create() {
                let block = Core;
                block.create();
//                $name
            }
            // Все имена полей
            fn field_names() -> &'static [&'static str] {
                static NAMES: &'static [&'static str] = &[$(stringify!($fname)),*];
                NAMES
            }
            // Типы полей
            fn field_types() -> &'static [&'static str] {
                static TYPES: &'static [&'static str] = &[$(stringify!($ftype)),*];
                TYPES
            }
        }
    }
}


//
pub struct Core<T> {
    models: T
}

pub trait Module {
    fn create<'b>(&'b self);
}

impl <T> Module for Core<T> {
    fn create<'b>(&'b self)
    {
        let model: T = self.models;
        // Все поля модели
        let mut field_model: Vec<String> = vec![];
        // Типы полей
        let mut types_model: Vec<String> = vec![];

 'name: for el_name in &model.field_names(){
            &field_model.push(el_name.to_string());
    'types: for el_type in &model.field_types(){
                &types_model.push(el_type.to_string());
                continue 'name;
            }
        }

        // Поля
        let field = fields::Fields{
            name: field_model,
            types: types_model,
        };
        // Таблица
        let table = table::Table{
            name: "block".to_string(),
            fields: field,
        };
        table.create();
        println!("Core initialize");
    }
}
