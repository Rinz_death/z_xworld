// Структура которая будет хранить поля
pub struct Fields {
    pub name: Vec<String>,
    pub types: Vec<String>,
}

// Какие событие будет поддерживать поле
pub trait ModuleField {
    fn view<'b>(&'b self) -> String;
}

// Форматируем поля для их создания
impl<'a> ModuleField for Fields {
    fn view<'b>(&'b self) -> String{
        let mut string_field: String = "".to_string();
        let mut iter: i32 = 0;
        'names:for name in &self.name {
            iter += 1;
            'types:for types in &self.types {
                if self.name.len() > iter as usize {
                    string_field += &format!("{} {}, ",name,types);
                    continue 'names;
                } else {
                    string_field += &format!("{} {}",name,types);
                    continue 'names;
                }
            }
        }
        return string_field
    }
}

