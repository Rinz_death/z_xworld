extern crate rusqlite;
use self::rusqlite::Connection;
//extern crate time;
use core::database::fields::Fields;
use core::database::fields::ModuleField;
// Структура с таблицами
pub struct Table {
    pub name: String,
    pub fields: Fields,
}

// Какие событие будет поддерживать таблица
pub trait ModuleTable {
    fn create<'b>(&'b self);
}

impl<'a> ModuleTable for Table {
    fn create<'b>(&'b self){
        println!("ORM initialize");
        // Соединение с БД
        let conn = Connection::open("block.sqlite3").unwrap();
        // Переводим поля в строку
        let _fields = self.fields.view();
        let _query = format!("CREATE TABLE {} (id INTEGER PRIMARY KEY, {})", self.name, _fields);
        // Выполняем запрос к БД
        conn.execute(&_query,&[]).unwrap();
    }
}

